//
//  RestaurantResponseHandler.swift
//  cc-assignment
//
//  Created by DaniyalJavaid on 10/03/2019.
//  Copyright © 2019 DaniyalJavaid. All rights reserved.
//

import Foundation
import SwiftyJSON
protocol RestaurantDelegate:class {
    func onRestaurantsDataSuccess(response:JSON)
}
