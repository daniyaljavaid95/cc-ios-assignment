//
//  Http.swift
//  cc-assignment
//
//  Created by DaniyalJavaid on 09/03/2019.
//  Copyright © 2019 DaniyalJavaid. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON
class UtilHttp{
    
    var delegate:HttpDelegate!
    
    public func request(url:String,requestCode:String) {
        Alamofire.request(url)
            .responseJSON { response in
                self.delegate.onSuccess(response:JSON(response.result.value), requestCode:requestCode)
                //print(JSON(response.result.value))
        }
    }
}
