//
//  ViewController.swift
//  cc-assignment
//
//  Created by DaniyalJavaid on 09/03/2019.
//  Copyright © 2019 DaniyalJavaid. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON

class ViewController: UIViewController, WeatherDelegate, RestaurantDelegate, LocationDelegate {
    
    
    let locationManager = CLLocationManager()
    var weatherAPI = WeatherModule()
    var restaurantAPI = RestaurantModule()
    let http = UtilHttp()
    var currentLocation:LocationModule!
    override func viewDidLoad() {
        super.viewDidLoad()
        currentLocation = LocationModule(controller:self)
        weatherAPI = WeatherModule(delegate:self)
        restaurantAPI = RestaurantModule(delegate:self)
        weatherAPI.getCurrentWeatherDetails()
        weatherAPI.getWeatherForecast()
        restaurantAPI.getRestaurantsData(coordinates: "24.9411965,67.0762584")//this is to be called after getting currentCoordinates
        currentLocation.getLocation()
    }
    
    func onGetCoordinatesSuccess(coor: String) {
        print("onGetCoordinatesSuccess called from view controller")
        print(coor)
    }
    func onForecastResponseSuccess(response:JSON){
        print("onForecastResponseSuccess called from view controller")
        print(response)
    }
    func onCurrentWeatherResponseSuccess(response:JSON){
        print("onCurrentWeatherResponseSuccess called from view controller")
        print(response)
    }
    func onRestaurantsDataSuccess(response:JSON){
        print("onRestaurantsDataSuccess called from view controller")
        print(response)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

