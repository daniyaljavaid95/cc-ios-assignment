//
//  Weather.swift
//  cc-assignment
//
//  Created by DaniyalJavaid on 09/03/2019.
//  Copyright © 2019 DaniyalJavaid. All rights reserved.
//

import Foundation
import SwiftyJSON
class WeatherModule: HttpDelegate{
    private var constants = Constants()
    private var httpReq = UtilHttp()
    var delegate:WeatherDelegate!
    init(){
    }
    init(delegate:WeatherDelegate){
        self.delegate = delegate
        httpReq.delegate = self
    }
    
    
    func onSuccess(response:JSON, requestCode:String){
        if(requestCode == constants.currentWeatherReqCode){
            self.delegate.onCurrentWeatherResponseSuccess(response: response)
        }
        else if(requestCode == constants.forecastWeatherReqCode){
            self.delegate.onForecastResponseSuccess(response: response)
        }
    }
    
    public func getCurrentWeatherDetails(){
        httpReq.request(url: constants.currentWeatherURL,requestCode: constants.currentWeatherReqCode)
    }
    
    public func getWeatherForecast(){
        httpReq.request(url: constants.forecastWeatherURL,requestCode: constants.forecastWeatherReqCode)
    }
}
