//
//  Restaurant.swift
//  cc-assignment
//
//  Created by DaniyalJavaid on 09/03/2019.
//  Copyright © 2019 DaniyalJavaid. All rights reserved.
//

import Foundation
import SwiftyJSON

class RestaurantModule: HttpDelegate{
    private var constants = Constants()
    private var httpReq = UtilHttp()
    var delegate:RestaurantDelegate!
    init(){
    }
    init(delegate:RestaurantDelegate){
        self.delegate = delegate
        httpReq.delegate = self
    }
    func onSuccess(response:JSON, requestCode:String){
        if(requestCode == constants.restaurantsReqCode){
            self.delegate.onRestaurantsDataSuccess(response: response)
        }
    }
    public func getRestaurantsData(coordinates:String){
        print("Restaurant URL => \(constants.getRestaurantApiURL(coordiantes: ""))")
        httpReq.request(url: constants.getRestaurantApiURL(coordiantes: coordinates),requestCode: constants.restaurantsReqCode)
    }
}
