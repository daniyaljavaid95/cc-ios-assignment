//
//  Location.swift
//  cc-assignment
//
//  Created by DaniyalJavaid on 10/03/2019.
//  Copyright © 2019 DaniyalJavaid. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
class LocationModule:NSObject, CLLocationManagerDelegate{
    var uiController:ViewController!
    let locationManager = CLLocationManager()
    var delegate:LocationDelegate!
    init(controller:ViewController){
        self.uiController = controller
        self.delegate = controller
    }
    func getLocation(){
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest // You can change the locaiton accuary here.
            locationManager.startUpdatingLocation()
        }
    }
    // Print out the location to the console
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.delegate.onGetCoordinatesSuccess(coor: "\(location.coordinate)")
        }
    }
    
    // If we have been deined access give the user the option to change it
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }
    
    // Show the popup to the user if we have been deined access
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Background Location Access Disabled",
                                                message: "In order to show top restaurants we need your location",
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        uiController.present(alertController, animated: true, completion: nil)
    }
}
