//
//  Constants.swift
//  cc-assignment
//
//  Created by DaniyalJavaid on 09/03/2019.
//  Copyright © 2019 DaniyalJavaid. All rights reserved.
//

import Foundation
class Constants{
    private var weatherApiKey = "7bffca4de14d44c6b28115337190903"
    private var weatherApiBaseURL = "http://api.apixu.com/v1/"
    private var weatherApiParamDaysCount = "&days=5"
    private var weatherApiParamLocation = "&q=Karachi"
    private var weatherApiCurrentData = "current.json"
    private var weatherApiForecastData = "forecast.json"
    private var gmapApiBaseURL = "https://maps.googleapis.com/maps/api/place/textsearch/json"
    private var gmapApiKey = "AIzaSyCSbNG0PLVjgKMEwGTOkubXlFfljGdFhtE"
    private var gmapApiQuery = "?query=5star+restaurant"
    
    public var currentWeatherReqCode = "111"
    public var forecastWeatherReqCode = "222"
    public var restaurantsReqCode = "333"
    
    private var restaurantsURL=""
    var currentWeatherURL: String
    var forecastWeatherURL: String
    
    init(){
        self.currentWeatherURL = "\(weatherApiBaseURL)\(weatherApiCurrentData)?key=\(weatherApiKey)\(weatherApiParamLocation)"
        self.forecastWeatherURL = "\(weatherApiBaseURL)\(weatherApiForecastData)?key=\(weatherApiKey)\(weatherApiParamLocation)\(weatherApiParamDaysCount)"
    }
    
    public func getRestaurantApiURL(coordiantes:String)->String{
        let url = "\(gmapApiBaseURL)\(gmapApiQuery)&location=\(coordiantes)&key=\(gmapApiKey)"
        self.restaurantsURL = url
        return url
    }
    
}
